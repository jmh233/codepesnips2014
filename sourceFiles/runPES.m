
addpath ../GPstuff-4.4/diag
addpath ../GPstuff-4.4/dist
addpath ../GPstuff-4.4/gp
addpath ../GPstuff-4.4/mc
addpath ../GPstuff-4.4/misc
addpath ../GPstuff-4.4/optim
addpath ../GPstuff-4.4/xunit
addpath ../sourceFiles

% We load the simulation index

simulation = load('simulation.txt');

% We load the values needed to evaluate the objective function

% the objective function to be minmized

objective = @(x) target(x);

% The number of samples from the global optimum to be drawn, the boundaries and the number of random features

nM = 200;
xmin = [ 0 ; 0 ];
xmax = [ 1 ; 1 ];
nFeatures = 1000;

% We initialize the random seed

s = RandStream('mcg16807','Seed', simulation * 10000);
RandStream.setGlobalStream(s);

% We obtain three random samples

nInitialSamples = 3;
Xsamples = lhsu(xmin, xmax, nInitialSamples);
guesses = Xsamples;
Ysamples = zeros(nInitialSamples, 1);
for i = 1 : nInitialSamples
	Ysamples(i) = objective(Xsamples(i,:));
end
start = nInitialSamples + 1;

% We sample from the posterior distribution of the hyper-parameters

[ l, sigma, sigma0 ] = sampleHypers(Xsamples, Ysamples, nM);

for i = start : 30

	% We initialize the random seed

	s = RandStream('mcg16807','Seed', simulation * 1000 + i );
	RandStream.setGlobalStream(s);

	% We sample from the global minimum

	[ m hessians ] = sampleMinimum(nM, Xsamples, Ysamples, sigma0, sigma, l, xmin, xmax, nFeatures);

	% We call the ep method

	ret = initializeEPapproximation(Xsamples, Ysamples, m, l, sigma, sigma0, hessians);

	% We define the cost function to be optimized

	cost = @(x) evaluateEPobjective(ret, x);

	% We optimize globally the cost function

	optimum = globalOptimizationOneArgument(cost, xmin, xmax, guesses);

	% We collect the new measurement
	
	Xsamples = [ Xsamples ; optimum ];

	Ysamples = [ Ysamples ; objective(optimum) ];

	% We sample from the posterior distribution of the hyper-parameters

	[ l, sigma, sigma0 ] = sampleHypers(Xsamples, Ysamples, nM);

	% We update the kernel matrix on the samples

	KernelMatrixInv = {};
	for j = 1 : nM
		KernelMatrix = computeKmm(Xsamples, l(j,:)', sigma(j), sigma0(j));
		KernelMatrixInv{ j } = chol2invchol(KernelMatrix);
	end

	f = @(x) posteriorMean(x, Xsamples, Ysamples, KernelMatrixInv, l, sigma);
	gf = @(x) gradientPosteriorMean(x, Xsamples, Ysamples, KernelMatrixInv, l, sigma);

	% We optimize the posterior mean of the GP
		
	optimum = globalOptimization(f, gf, xmin, xmax, guesses);

	guesses = [ guesses ; optimum ];

	if ~exist('results', 'dir')
	    mkdir('results');
    end
	dlmwrite('results/Xsamples.txt', Xsamples, 'delimiter', ' ', 'precision', '%1.40f');
	dlmwrite('results/Ysamples.txt', Ysamples, 'delimiter', ' ', 'precision', '%1.40f');
	dlmwrite('results/guesses.txt', guesses, 'delimiter', ' ', 'precision', '%1.40f');

	fprintf(1, '%d\n', i);
end
